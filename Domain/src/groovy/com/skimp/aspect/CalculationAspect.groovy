package com.skimp.aspect
import groovy.util.logging.Log4j
import org.aspectj.lang.annotation.AfterReturning
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import org.springframework.stereotype.Component
/**
 * Created by Claudio Carcaci on 28 Jul 2016.
 */

// @Component annotation is useful to auto detect this class as a bean instead of specify this into resources.groovy of each project that needs to use this aspect
// @see http://docs.spring.io/spring/docs/current/spring-framework-reference/html/aop.html#aop-at-aspectj
// Also add the package com.skimp.aspect into Config.groovy->grails.spring.bean.packages setting
@Component
// This specify that the class is an aspect
@Aspect
@Log4j
class CalculationAspect {
	// Pointcuts
	@Pointcut("@annotation(com.skimp.annotation.ProductPriceCalculation)")
	public void productPriceSuggestionCalculation() {	}

	// Advices
	@AfterReturning("productPriceSuggestionCalculation()")
	public void productPriceSuggestionCalculator() {
		// Todo
		// Todo try use Around advice in order to make the controller returning before the
		// Todo if is not possible then instantiate new thread where to run the update
		Thread.start({
			Thread.sleep(5000)
			log.info("foo")
		})
	}
}
