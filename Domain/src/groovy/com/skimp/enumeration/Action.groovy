package com.skimp.enumeration
/**
 * Created by Claudio Carcaci on 11 May 2016.
 */
enum Action {
	NEW, DELETED, UPDATED, CHECKED
}
