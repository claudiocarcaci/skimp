package com.skimp.enumeration
/**
 * Created by Claudio Carcaci on 11 May 2016.
 */
enum State {
	TO_PROCESS, PROCESSED, CONFIRMED
}
