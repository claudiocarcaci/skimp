package com.skimp

class Brand {
   String name
   Thumb logo

   static constraints = {
      logo nullable: true
   }
}
