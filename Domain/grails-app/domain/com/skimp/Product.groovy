package com.skimp

class Product {
   String name
   String description
   Brand brand
   List<ProductLocationSuggestion> productLocationSuggestions
   List<ProductPriceSuggestion> productPriceSuggestions
   List<ProductSpecSuggestion> productSpecSuggestions
   List<ProductTagSuggestion> productTagSuggestions

   static transients = [ "productLocationSuggestions", "productPriceSuggestions", "productSpecSuggestions", "productTagSuggestions" ]

   static hasMany = [ProductLocationSuggestion, ProductPriceSuggestion, ProductSpecSuggestion, ProductTagSuggestion]

   static constraints = {
      name nullable: false
      description nullable: true
      brand nullable: true
   }

   static mapping = {
      description type: "text" //longtext
   }
}
