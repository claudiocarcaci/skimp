package com.skimp

import com.skimp.enumeration.Action
import com.skimp.enumeration.State

/**
 * Created by Claudio Carcaci on 28/10/2015.
 */
class Insertion {
   Seller seller
   Product product
   Store store
   Price price

	Action action
	State state
}
