package com.skimp

class ZipCode {
   String city
   String province
   String phonePrefix
   String zipCode
   //City and zipCode are indexed in order to improve search speed
   static mapping = {
      city index:"city_index"
      zipCode index:"zipCode_index"
   }
}
