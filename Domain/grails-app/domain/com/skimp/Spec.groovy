package com.skimp

class Spec {
   String name
   String value

   static constraints = {
      value nullable: true
   }
}
