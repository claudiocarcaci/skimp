package com.skimp

class Picture {
   String name
   String description
   Image image
   Product product
   static constraints = {
      name nullable: true
      description nullable: true
      product nullable: true
   }
   static mapping = {
      description type:"text"
   }
}
