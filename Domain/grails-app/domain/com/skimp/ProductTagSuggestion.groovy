package com.skimp

import com.skimp.enumeration.Action
import com.skimp.enumeration.State

class ProductTagSuggestion {
   String rawTag
   Tag tag
   String rawProductName
   Product product
   Person person

	Action action
	State state

   static constraints = {
      tag nullable: true
      rawProductName nullable: true
      product nullable: true
   }
}
