package com.skimp

import com.skimp.enumeration.Action
import com.skimp.enumeration.State

class ProductLocationSuggestion {
   String productName
   String productLocation

	Action action
	State state

	Product product
	Store store
	Person person

   static belongsTo = [ProductPriceSuggestion]

   static constraints = {
      productName nullable: true
      product nullable: true
      productLocation nullable: true
      store nullable: true
   }
}
