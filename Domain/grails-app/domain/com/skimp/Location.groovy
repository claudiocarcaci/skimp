package com.skimp

class Location {
    Double latitude
    Double longitude
    static constraints = {
        latitude nullable: true
        longitude nullable: true
    }
}
