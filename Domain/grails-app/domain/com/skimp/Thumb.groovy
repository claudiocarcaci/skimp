package com.skimp

class Thumb extends Picture {
   Picture picture
   Product product
   static constraints = {
      product nullable: true
   }
}
