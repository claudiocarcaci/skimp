package com.skimp

class Address {
    String streetType
    String streetName
    String streetNumber
    String floorNumber
    ZipCode zipCode
    Location location

   public String getCompleteStreet() {
      return "$streetType $streetName $streetNumber"
   }

   public String getCompleteCity() {
      return "${zipCode.getCity()} ${zipCode.getZipCode()} (${zipCode.getPhonePrefix()})"
   }

   public String getCompleteAddress() {
      return "${getCompleteStreet()}, ${getCompleteCity()}"
   }
}
