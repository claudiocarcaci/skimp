package com.skimp

/**
 * Created by Claudio Carcaci on 28/10/2015.
 */
class Contact {
   enum Type {HOME,WORK}
   enum MediaType {PHONE,MOBILE,EMAIL,WEBSITE,CUSTOM}
   Type type
   MediaType mediaType
   String customMediaType
   String contact
   Actor actor
   Store store
}
