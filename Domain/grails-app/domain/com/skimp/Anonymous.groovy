package com.skimp

class Anonymous {
   String fingerprint
   String sessionId
   Actor actor

   static constraints = {
      fingerprint nullable: true
      sessionId nullable: true
      actor nullable: true
   }
}
