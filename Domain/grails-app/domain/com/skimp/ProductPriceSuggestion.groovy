package com.skimp

import com.skimp.enumeration.Action
import com.skimp.enumeration.State

class ProductPriceSuggestion {
   String productName
   String productBrand
   Image productImage
   Price price
   Date dateCreated

	Action action
	State state

   Product product
   Person person
   ProductLocationSuggestion productLocationSuggestion

   static constraints = {
      productName nullable: true
      productBrand nullable: true
      productImage nullable: true
      price nullable: true
      product nullable: true
      productLocationSuggestion nullable: true

		// Todo remove the line below and set up Jwt auth ASAP
		person nullable: true
   }
}
