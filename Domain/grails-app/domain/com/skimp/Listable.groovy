package com.skimp

import com.skimp.uac.User

/**
 * Created by Claudio Carcaci on 27/10/2015.
 */
abstract class Listable {
   User user
   static hasMany = [products: Product]
   static belongsTo = Product
}
