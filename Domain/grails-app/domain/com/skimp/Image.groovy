package com.skimp

class Image {
   enum ImageFormat { PNG, JPG }
   String cdnUrl
   ImageFormat format
   Integer width
   Integer height
   Integer bitDepth
}
