package com.skimp

import com.skimp.enumeration.Action
import com.skimp.enumeration.State

class ProductSpecSuggestion {
   String rawProductName
   Product product
   String rawSpecName
   String rawSpecValue
   Spec spec
   Person person

	Action action
	State state

   static constraints = {
      rawProductName nullable: true
      product nullable: true
      rawSpecValue nullable: true
      spec nullable: true
   }
}
