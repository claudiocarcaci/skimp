package com.skimp

/**
 * Created by Claudio Carcaci on 27/10/2015.
 */
class Person extends Actor {
   String firstName
   String middleName
   String lastName
   String secondLastName

   static constraints = {
      firstName nullable: true
      middleName nullable: true
      lastName nullable: true
      secondLastName nullable: true
   }
}
