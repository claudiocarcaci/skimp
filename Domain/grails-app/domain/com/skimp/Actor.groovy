package com.skimp
/**
 * Created by Claudio Carcaci on 27/10/2015.
 */
abstract class Actor {
   String email
   String vatCode
   Contact contact

   static mapping = {
      tablePerHierarchy(false)
   }

   static constraints = {
      email nullable: true
      vatCode nullable: true
      contact nullable: true
   }
}
