package com.skimp

import cr.co.arquetipos.currencies.Money

/**
 * Created by Claudio Carcaci on 28/10/2015.
 */
class Price {
   Money amount
   Date dateCreated
   Date lastUpdated

   public String getFormattedAmount() {
      return "${amount.getCurrency().getSymbol()} ${amount.getAmount()}"
   }
}
