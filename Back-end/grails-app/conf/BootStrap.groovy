import com.skimp.uac.Role
import groovy.util.logging.Log4j

@Log4j
class BootStrap {
   def init = { servletContext ->
      // Disable full stacktrace printing by setting JVM flag
      System.setProperty("grails.full.stacktrace","false")

      // Init the system, create useful roles if them don't exist
      List<Role> allRoles = Role.findAll()
      Role roleAdmin = allRoles.find { it.getAuthority()=="ROLE_ADMIN" }
      Role rolePerson = allRoles.find { it.getAuthority()=="ROLE_PERSON" }
      Role roleSeller = allRoles.find { it.getAuthority()=="ROLE_SELLER" }
      Role roleSupervisor = allRoles.find { it.getAuthority()=="ROLE_SUPERVISOR" }

      if(roleAdmin == null) {
         log.info("Role admin doesn't exist during bootstrap, create it")
         new Role(authority: "ROLE_ADMIN").save(failOnError: true)
      }

      if(rolePerson == null) {
         log.info("Role person doesn't exist during bootstrap, create it")
         new Role(authority: "ROLE_PERSON").save(failOnError: true)
      }

      if(roleSeller == null) {
         log.info("Role seller doesn't exist during bootstrap, create it")
         new Role(authority: "ROLE_SELLER").save(failOnError: true)
      }

      if(roleSupervisor == null) {
         log.info("Role supervisor doesn't exist during bootstrap, create it")
         new Role(authority: "ROLE_SUPERVISOR").save(failOnError: true)
      }
   }
   def destroy = {
   }
}
