import com.skimp.annotations.ActorIsMandatoryAspect
import com.skimp.security.authenticationlistener.BindOnLoginEventListener

// Place your Spring DSL code here
beans = {
   //Inject Spring singleton bean that implements @ActorIsMandatory annotation aspect
   actorIsMandatoryAspect(ActorIsMandatoryAspect)

   bindOnLoginEventListener(BindOnLoginEventListener)
}
