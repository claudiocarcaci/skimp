package com.skimp

import com.skimp.uac.Role
import com.skimp.uac.User
import groovy.util.logging.Log4j

/**
 * Created by Claudio Carcaci on 24 Dec 2015.
 */
@Log4j
class ActorCreator {
   Actor createActor(String email, User user, Role.Roles role) {
      Person person
      Seller seller
      Supervisor supervisor

      switch(role) {
         case Role.Roles.ROLE_PERSON:
            person = new Person(email: email)
            user.setActor(person)

            return person
         case Role.Roles.ROLE_SELLER:
            seller = new Seller(email: email)
            user.setActor(seller)

            return seller
         case Role.Roles.ROLE_SUPERVISOR:
            supervisor = new Supervisor(email: email)
            user.setActor(supervisor)

            return supervisor
         default:
            log.info("Role ${role.toString()} is invalid")
      }
   }

   Actor createAnonymous(Anonymous anonymous) {
      Person person = new Person()
      anonymous.setActor(person)

      return person
   }
}
