package com.skimp.security.authenticationlistener
import com.skimp.ActorService
import grails.util.Holders
import groovy.util.logging.Log4j
import org.codehaus.groovy.grails.commons.GrailsApplication
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.context.ApplicationListener
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.authentication.event.AuthenticationSuccessEvent
import org.springframework.web.context.request.RequestContextHolder
/**
 * Created by Claudio Carcaci on 20 Feb 2016.
 */
@Log4j
class BindOnLoginEventListener implements ApplicationListener<AuthenticationSuccessEvent> {
   GrailsApplication grailsApplication = Holders.grailsApplication
   ActorService actorService = grailsApplication.mainContext.getBean("actorService")

   //When an user does a login then merge information about actions that the user did as anonymous
   @Override
   void onApplicationEvent(AuthenticationSuccessEvent event) {
      Long userId = ((AbstractAuthenticationToken)event.getSource()).getPrincipal().getId()
      String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId()
      String fingerprint = ((GrailsParameterMap)RequestContextHolder.currentRequestAttributes().params).fingerprint

      actorService.bindUserAndAnonymous(userId, sessionId, fingerprint)
   }
}
