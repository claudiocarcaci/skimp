package com.skimp.annotations
import com.skimp.ActorService
import com.skimp.Anonymous
import com.skimp.Person
import com.skimp.uac.User
import grails.plugin.springsecurity.SpringSecurityService
import grails.util.Holders
import groovy.util.logging.Log4j
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.codehaus.groovy.grails.commons.GrailsApplication
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.web.context.request.RequestContextHolder
/**
 * Created by Claudio Carcaci on 1 Feb 2016.
 */
@Aspect
@Log4j
//Define bean that will implement the aspect (see resources.groovy)
// This aspect is useful to ensure that each register and logged in user has an associated person
// Hence, otherwise, if the user isn't logged in and register this aspect will associate session id and fingerprint with an anonymous user, by creating it if needed
class ActorIsMandatoryAspect {
   GrailsApplication grailsApplication = Holders.grailsApplication
   ActorService actorService = grailsApplication.mainContext.getBean("actorService")
   SpringSecurityService springSecurityService = grailsApplication.mainContext.getBean("springSecurityService")

   //Runs this advice when method with specified annotation is called
   @Before("@annotation(ActorIsMandatory)")
   // This is the Advice
   public void actorIsMandatory() {
      Person person

      if(springSecurityService.isLoggedIn()==true) {
         User user = springSecurityService.getCurrentUser()

         if(user.getActor()==null) {
            person = new Person().save(failOnError: true)
            user.setActor(person)
         }
      } else {
         String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId()
         String fingerprint = ((GrailsParameterMap)RequestContextHolder.currentRequestAttributes().params).fingerprint
         Anonymous anonymous = Anonymous.findBySessionIdOrFingerprint(sessionId, fingerprint)

         if(anonymous == null) {
            person = new Person().save(failOnError: true)
            new Anonymous(sessionId: sessionId, fingerprint: fingerprint, actor: person).save(failOnError: true)
         }
      }
   }
}
