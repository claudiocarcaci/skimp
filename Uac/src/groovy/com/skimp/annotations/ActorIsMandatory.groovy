package com.skimp.annotations

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

/**
 * Created by Claudio Carcaci on 30 Jan 2016.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
//Define annotation
public @interface ActorIsMandatory {
}
