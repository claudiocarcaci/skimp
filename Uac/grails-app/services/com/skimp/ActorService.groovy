package com.skimp

import grails.transaction.Transactional
import groovy.util.logging.Log4j
import com.skimp.uac.Role
import com.skimp.uac.User
import com.skimp.uac.UserRole

//Service related to Actors: Seller, Person, Supervisor and User (UAC) domain classes
@Log4j
class ActorService {
   @Transactional
   Actor createActor(String email,String password) {
      //Create new person as default behavior
      return createActor(email, password, Role.Roles.ROLE_PERSON)
   }

   @Transactional
   Actor createActor(String email,String password, Role.Roles selectedRole) {
      User user = new User(username: email, password: password)
      Role role = Role.findByAuthority(selectedRole.toString())
      UserRole userRole = new UserRole(user: user, role: role)
      //Associate the user to created actor using factory object
      Actor actor = new ActorCreator().createActor(email, user, selectedRole)
      actor.save(failOnError: true)
      user.save(failOnError: true)
      userRole.save(failOnError: true)

      return actor
   }

   @Transactional
   Actor bindUserAndAnonymous(Long userId, String sessionId, String fingerprint) {
      log.info("$userId $sessionId $fingerprint")
   }

   @Transactional
   Actor savePersonInformation(User user, String firstName, String middleName, String lastName, String secondLastName) {
      Person person = user.getActor()

      person.setFirstName(firstName)
      person.setMiddleName(middleName)
      person.setLastName(lastName)
      person.setSecondLastName(secondLastName)
   }
}
