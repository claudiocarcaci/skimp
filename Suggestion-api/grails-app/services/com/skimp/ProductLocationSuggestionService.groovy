package com.skimp

import grails.transaction.Transactional

class ProductLocationSuggestionService {
	def transactional = false

	ProductLocationSuggestion getProductLocationSuggestion(Long id) {
		return ProductLocationSuggestion.get(id)
	}

	@Transactional
	Long createProductLocationSuggestion(String productName, String productBrand, String productLocation) {

	}
}
