package com.skimp

import com.skimp.enumeration.Action
import com.skimp.enumeration.State
import cr.co.arquetipos.currencies.Money
import grails.transaction.Transactional

class ProductPriceSuggestionService {
	def transactional = false

	ProductPriceSuggestion getProductPriceSuggestion(Long id) {
		return ProductPriceSuggestion.get(id)
	}

	@Transactional
	ProductPriceSuggestion createProductPriceSuggestion(String productName, String productBrand, String productPrice) {
		Money money
		Price price = null

		if(productPrice != null) {
			money = Money.getInstance(productPrice)
			price = new Price(amount: money)
		}

		ProductPriceSuggestion productPriceSuggestion = new ProductPriceSuggestion(productName: productName, productBrand: productBrand, price: price, action: Action.NEW, state: State.TO_PROCESS)

		return productPriceSuggestion
	}

	@Transactional
	ProductPriceSuggestion updateProductPriceSuggestion(Long productPriceSuggestionId, String productName, String productBrand, String productPrice) {
		ProductPriceSuggestion productPriceSuggestion = ProductPriceSuggestion.get(productPriceSuggestionId)

		if(productPriceSuggestion != null) {
			Money money = Money.getInstance(productPrice)
			Price price = new Price(amount: money)
			productPriceSuggestion.setProductName(productName)
			productPriceSuggestion.setProductBrand(productBrand)
			productPriceSuggestion.setPrice(price)
			productPriceSuggestion.setAction(Action.UPDATED)
			productPriceSuggestion.setState(State.TO_PROCESS)

			return productPriceSuggestion
		} else
			return null
	}

	@Transactional
	boolean deleteProductPriceSuggestion(Long productPriceSuggestionId) {
		ProductPriceSuggestion productPriceSuggestion = ProductPriceSuggestion.get(productPriceSuggestionId)

		if(productPriceSuggestion != null) {
			productPriceSuggestion.setAction(Action.DELETED)
			productPriceSuggestion.setState(State.TO_PROCESS)

			return true
		} else
			return false
	}
}
