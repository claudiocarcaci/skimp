package com.skimp
import com.skimp.annotation.ProductPriceCalculation
import grails.converters.JSON
import groovy.util.logging.Log4j

@Log4j
class ProductPriceSuggestionController {
	static responseFormats = ["json"]
	ProductPriceSuggestionService productPriceSuggestionService

	// http://localhost:8080/Suggestion-api/productPriceSuggestion?id={id}
	def index() {
		Long productPriceSuggestionId = params.long("id")

		ProductPriceSuggestion productPriceSuggestion = productPriceSuggestionService.getProductPriceSuggestion(productPriceSuggestionId)

		render productPriceSuggestion as JSON
	}

	// http://localhost:8080/Suggestion-api/productPriceSuggestion/create?productName={the-name}&productBrand={the-brand}&productPrice={the-price}
	@ProductPriceCalculation
	def create() {
		String productName = params.productName
		String productBrand = params.productBrand
		String productPrice = params.productPrice

		ProductPriceSuggestion productPriceSuggestion = productPriceSuggestionService.createProductPriceSuggestion(productName, productBrand, productPrice)

		render productPriceSuggestion as JSON
	}

	// http://localhost:8080/Suggestion-api/productPriceSuggestion/modify?id={id}&productName={the-name}&productBrand={the-brand}&productPrice={the-price}
	@ProductPriceCalculation
	def modify() {
		Long productPriceSuggestionId = params.long("id")
		String productName = params.productName
		String productBrand = params.productBrand
		String productPrice = params.productPrice

		ProductPriceSuggestion productPriceSuggestion = productPriceSuggestionService.updateProductPriceSuggestion(productPriceSuggestionId, productName, productBrand, productPrice)

		render productPriceSuggestion as JSON
	}

	// http://localhost:8080/Suggestion-api/productPriceSuggestion/remove/{id}
	@ProductPriceCalculation
	def remove() {
		Long productPriceSuggestionId = params.long("id")

		log.info("$productPriceSuggestionId")

		Boolean result = productPriceSuggestionService.deleteProductPriceSuggestion(productPriceSuggestionId)

		render result
	}
}
