package com.skimp

import grails.rest.RestfulController
import com.skimp.ProductLocationSuggestion

class ProductLocationSuggestionController extends RestfulController {
	static responseFormats = ["json"]

	public ProductLocationSuggestionController() {
		super(ProductLocationSuggestion)
	}

	def index() {}
}
